﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    [SerializeField]
    private AudioSource sfxSource = null;

    [SerializeField]
    private AudioSource bgmSource = null;
    [Header ("Audio clips")]
    [SerializeField]
    private AudioClip bgm = null;

    [SerializeField]
    private AudioSource[] bgmSources = new AudioSource[0];

    int samplePoint = 1190700;


    private void Start()
    {
       // PlayBgm(bgm);
        StartCoroutine(LoopBgmBySampleDualSource(bgm, samplePoint));
        
    }

    public void PlayOneShot(AudioClip clip) {
        if (clip)
        {
           // Debug.Log("Play audioclip "+ clip.name);
            sfxSource.PlayOneShot(clip);
        }
        else
        {
            Debug.Log("Audio clip missing");
        }
    }

    public void PlayOneShotWithVolumeChange(AudioClip clip, float volume)
    {
        if (clip)
        {
            // Debug.Log("Play audioclip "+ clip.name);
            sfxSource.PlayOneShot(clip, volume);
        }
        else
        {
            Debug.Log("Audio clip missing");
        }
    }

    public void PlayBgm(AudioClip clip) {
        if (clip)
        {
           // Debug.Log("Play audioclip " + clip.name);
            //bgmSource.clip = clip;
           // bgmSource.timeSamples = samplePoint;
            //bgmSource.Play();
            StartCoroutine(LoopBgmBySampleDualSource(bgm, samplePoint));
        }
        else
        {
            Debug.Log("Audio clip missing");
        }
    }

    public void FadeBgmVolume(float targetVolume, float timeToTake) {
        StartCoroutine(FadeVolumeFromToValue(bgmSource, bgmSource.volume, targetVolume, timeToTake));
    }

    IEnumerator FadeVolumeFromToValue(AudioSource source,float startValue, float targetValue, float timeTaken) {

        float diff = startValue - targetValue;
        float step = diff / (timeTaken / 0.1f);
        bool fading = true;
        while (fading)
        {
            if (startValue > targetValue)
            {
                source.volume = Mathf.Clamp(source.volume -= step, targetValue, startValue);
            }
            else
            {
                source.volume = Mathf.Clamp(source.volume -= step, startValue, targetValue);
            }
            if (source.volume == targetValue)
            {
                fading = false;
            }
            yield return new WaitForSeconds(0.1f);
        }
    
    }
    public void FadeBgmVolumeToAndBack(float targetVolume, float timeToTake, float timeBeforeFadingBack)
    {
        StartCoroutine(FadeVolumeFromToValueAndBack(bgmSource, targetVolume, timeToTake, timeBeforeFadingBack));
    }

    IEnumerator FadeVolumeFromToValueAndBack(AudioSource source, float targetValue, float fadeTime, float timeBeforeFadingBack)
    {
        float startValue = source.volume;
        float diff = source.volume - targetValue;
        float step = diff / (fadeTime / 0.1f);
        bool fading = true;
        bool faded = false;
        while (fading)
        {
            if (source.volume > targetValue)
            {
                source.volume = Mathf.Clamp(source.volume -= step, targetValue, 1);
            }
            else
            {
                source.volume = Mathf.Clamp(source.volume += step, 0, targetValue);
            }
            if (source.volume == targetValue)
            {
                if (!faded)
                {
                    targetValue = startValue;
                    faded = true;
                    yield return new WaitForSeconds(timeBeforeFadingBack);
                }
                else
                {
                    fading = false;
                }
               
                
            }
            yield return new WaitForSeconds(0.1f);
     
        }

    }

    IEnumerator LoopBgmBySampleDualSource(AudioClip clip, int loopSamplePoint)
    {
        double clipLenght = clip.length;
        foreach (var item in bgmSources)
        {
            item.loop = false;
            item.clip = clip;
        }
        int sourceInUse = 0;
        bgmSources[0].Play();
        bgmSources[1].timeSamples = loopSamplePoint;
        bgmSources[1].PlayScheduled(clipLenght);
        while (true)
        {
            if (!bgmSources[sourceInUse].isPlaying)
            {
                bgmSources[sourceInUse].Stop();
                bgmSources[sourceInUse].timeSamples = loopSamplePoint;
                
                if (sourceInUse == 0)
                {
                    sourceInUse = 1;
                    bgmSources[0].PlayScheduled(clipLenght - bgmSources[sourceInUse].time);
                }
                else
                {
                    sourceInUse = 0;
                    bgmSources[1].PlayScheduled(clipLenght - bgmSources[sourceInUse].time);
                }
                
            }
            yield return new WaitForEndOfFrame();
        }

    }
}
