﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RamLogic : MonoBehaviour
{
    public delegate void memEvent();
    public static memEvent OnMemMaxReached;
    public static memEvent OnMemWinReached;

    private float usedMem = 0;
    public float maxMem = 100;
    public float winMem = 0;

    public Animator ramAnimator;

    private bool lost = false;
    // Start is called before the first frame update
    void Start()
    {
        //Debug.Log("test" + usedMem);

    }

    private void OnEnable()
    {
        PopUpGenerator.OnPopUpOpened += AddMem;
        PopUpGenerator.OnPopUpClosed += ReduceMem;
        GameController.OnGameRestarted += OnRestart;
        GameController.OnGameRestartRamReset += RamReset;
    }

    private void OnDisable()
    {
        PopUpGenerator.OnPopUpOpened -= AddMem;
        PopUpGenerator.OnPopUpClosed -= ReduceMem;
        GameController.OnGameRestarted -= OnRestart;
        GameController.OnGameRestartRamReset -= RamReset;
    }

    // Adds the amount of mem in mem to add into the counter if it is positive
    public void AddMem(float memToAdd)
    {
        //Debug.Log("plussaa: " + memToAdd);
        if (memToAdd > 0)
        {
            usedMem += memToAdd;
            ramAnimator.SetFloat("usedmem",usedMem);
            //Debug.Log("total: "+ usedMem);
            if (usedMem >= maxMem)
            {
                MaxMemReached();
            }
        }
    }

    public void ReduceMem(float memToReduce)
    {
        if (lost)
            return;
        //Debug.Log("miinusta" + usedMem);
        // can afford to reduce mem
        if (memToReduce <= usedMem)
        {
            usedMem -= memToReduce;
            ramAnimator.SetFloat("usedmem", usedMem);
            
        }
        if (usedMem <= winMem)
        {
            WinMemReached();
        }
    }

    private void MaxMemReached()
    {
        //usedMem = 0;
        lost = true;
        OnMemMaxReached?.Invoke();
    }

    public void WinMemReached()
    {
        OnMemWinReached?.Invoke();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void RamReset()
    {
        usedMem = 0;
        lost = false;

    }

    void OnRestart()
    {
        usedMem = 0;
        lost = false;
        ramAnimator.SetFloat("usedmem", usedMem);
    }
}
