﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public delegate void GameEvent();
    public static GameEvent OnGameStarted;
    public static GameEvent OnGameRestarted;
    public static GameEvent OnGameRestartRamReset;
    public static GameEvent PowerOffEvent;
    public static GameEvent LookedAtPC;
    public static GameEvent TitlePressedplayEvent;

    public Difficulty chosenDifficulty;

    public void StartGame() {
        OnGameStarted?.Invoke();
    }

    public void LookAtComputer()
    {
        LookedAtPC?.Invoke();
        StartGame();
    }

    public void RestartGame() {
        OnGameRestartRamReset?.Invoke();
        OnGameRestarted?.Invoke();
        StartCoroutine(WaitForRestart());
    }

    IEnumerator WaitForRestart() {
        yield return new WaitForSeconds(1f);
        StartGame();
    }

    public void TitlePressedPlay()
    {
        TitlePressedplayEvent?.Invoke();
    }

    public void PowerOff()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        PowerOffEvent?.Invoke();

    }

}
