﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiController : MonoBehaviour
{

    public GameObject backgroundObject;

    public float waitForWinS = 3;

    //private Image bgKuva;


    [Header("Panels used:")]
    [SerializeField]
    private GameObject winPanel = null;
    [SerializeField]
    private GameObject winText = null;

    [SerializeField]
    private GameObject computerBorder = null;
    [SerializeField]
    private GameObject computerBGLoggedIn = null;
    [SerializeField]
    private GameObject computerBGLose = null;
    [SerializeField]
    private GameObject PCScreen = null;
    [SerializeField]
    private GameObject PCScreenOnImages = null;
    private Animator pcScreenreset = null;


    [SerializeField]
    private GameObject losePanel = null;

    [SerializeField]
    private GameObject ramMeter = null;
    private Image ramSprite;

    [SerializeField]
    private GameObject roomPanel = null;
    public float waitAtRoom = 3;
    [SerializeField]
    private GameObject panelRoomandBG = null;
    [SerializeField]
    private GameObject titlePanel = null;

    [SerializeField]
    private GameObject wallPanel = null;


    [Header("Audio used:")]

    private AudioManager audioManager;
    [SerializeField]
    private AudioClip winSound = null;
    [SerializeField]
    private AudioClip bgMusic = null;
    [SerializeField]
    private AudioClip loseSound = null;



    private void OnEnable()
    {
        RamLogic.OnMemMaxReached += Lose;
        RamLogic.OnMemWinReached += Win;
        GameController.OnGameStarted += StartGame;
        GameController.OnGameRestarted += OnRestart;
        GameController.LookedAtPC += LookComputer;
        GameController.TitlePressedplayEvent += HideTitleOpenRoom;
    }
    private void OnDisable()
    {
        RamLogic.OnMemMaxReached -= Lose;
        RamLogic.OnMemWinReached -= Win;
        GameController.OnGameStarted -= StartGame;
        GameController.OnGameRestarted -= OnRestart;
        GameController.LookedAtPC -= LookComputer;
        GameController.TitlePressedplayEvent -= HideTitleOpenRoom;

    }
    // Start is called before the first frame update
    void Start()
    {
        audioManager = GameObject.FindGameObjectWithTag("AudioManager").GetComponent<AudioManager>();
        if (backgroundObject != null)
        {
            //bgKuva = backgroundObject.GetComponent<Image>();
            ramSprite = ramMeter.GetComponent<Image>();
            ramSprite.enabled = false;
        }
        pcScreenreset = PCScreenOnImages.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void HideTitleOpenRoom()
    {
        titlePanel.SetActive(false);
        panelRoomandBG.SetActive(true);
        StartCoroutine(RoomWait());
    }

    private void Lose()
    {
        if (audioManager) audioManager.PlayOneShot(loseSound);

        computerBGLose.SetActive(true);
        //Debug.Log("lost game");
        //computerBGLoggedIn.SetActive(false);
        //bgKuva.sprite = loseSprite;
        ramSprite.enabled = false;
        //ramMeter.SetActive(false);
        StartCoroutine(loseWait());
    }

    private void StartGame()
    {
        if(!computerBGLoggedIn.activeSelf)
        {
            computerBGLoggedIn.SetActive(true);

        }
        //bgKuva.sprite = bgSprite;
    }



    private void Win()
    {
        StartCoroutine(WinAnimation());
        //TODO: lisää voittoruutu
    }

    private void LookComputer()
    {
        wallPanel.SetActive(true);
        PCScreen.SetActive(true);
        computerBorder.SetActive(true);
        roomPanel.SetActive(false);
        //ramMeter.SetActive(true);
        ramSprite.enabled = true;
        //bgKuva.sprite = wallSprite;

    }

    private IEnumerator loseWait()
    {
        yield return new WaitForSeconds(waitAtRoom);
        losePanel.SetActive(true);
    }
    private IEnumerator RoomWait()
    {
        yield return new WaitForSeconds(waitAtRoom);
        roomPanel.SetActive(true);
    }
    private IEnumerator WinAnimation()
    {
        if (audioManager) audioManager.FadeBgmVolumeToAndBack(0f, waitForWinS, waitForWinS*3);
        yield return new WaitForSeconds(waitForWinS);
        if (audioManager) audioManager.PlayOneShot(winSound);
        winPanel.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        winText.SetActive(true);

        //TODO: change fade to different function
    }

    void OnRestart()
    {
        pcScreenreset.SetTrigger("resetTrigger");
        winText.SetActive(false);
        winPanel.SetActive(false);
        computerBGLoggedIn.SetActive(true);
        losePanel.SetActive(false);
        computerBGLose.SetActive(false);
        ramSprite.enabled = true;
    }
}
