﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopUpGenerator : MonoBehaviour
{
    public delegate void PopUpRamEvent(float ramAmount);
    public static PopUpRamEvent OnPopUpOpened;
    public static PopUpRamEvent OnPopUpClosed;

    [SerializeField]
    private List<GameObject> popUpPrefabs = new List<GameObject>();

    [SerializeField]
    private int popUpsAtStart = 10;

    private int numberOfPopUps = 0;

    private List<PopUpTypeHandler> activePopUps = new List<PopUpTypeHandler>();
    private List<PopUpTypeHandler> deactivePopUps = new List<PopUpTypeHandler>();

    [Header ("Pop up Speed")]
    [SerializeField]
    private float startPopUpSpeed = 0.3f;
    [SerializeField]
    private float popUpSpeedReduction = 0.01f;
    private float currentPopUpSpeed = 0;
    [SerializeField]
    private float fastestPopUpSpeed = 0.3f;
    [SerializeField]
    private float slowestPopUpSpeed = 0.8f;

    private bool allowPopUpCreation;
    private bool startPopsGenerated = false;
    private bool coroutineActive = false;

    private GameController gameController;

    private void Start()
    {
        gameController = GetComponentInParent<GameController>();
    }

    // Start is called before the first frame update
    void StartGame()
    {
        StopAllCoroutines();
        if(gameController) SetDifficulty();
        allowPopUpCreation = true;
        
        for (int i = 0; i < popUpsAtStart; i++)
        {
            CreatePopUp();
        }
        currentPopUpSpeed = startPopUpSpeed;
       
        startPopsGenerated = true;

    }

    void SetDifficulty() {
        if (gameController.chosenDifficulty)
        {
            popUpsAtStart = gameController.chosenDifficulty.popUpsAtStart;
            startPopUpSpeed = gameController.chosenDifficulty.startPopUpSpeed;
            popUpSpeedReduction = gameController.chosenDifficulty.popUpSpeedReduction;
            fastestPopUpSpeed = gameController.chosenDifficulty.fastestPopUpSpeed;
            slowestPopUpSpeed = gameController.chosenDifficulty.slowestPopUpSpeed;

            popUpPrefabs = gameController.chosenDifficulty.popUpPrefabs;
        }
    
    }
    private void OnEnable()
    {
        RamLogic.OnMemMaxReached += StopPopups;
        RamLogic.OnMemWinReached += StopPopups;
        GameController.OnGameStarted += StartGame;
        GameController.OnGameRestarted += OnRestart;
    }
    private void OnDisable()
    {
        RamLogic.OnMemMaxReached -= StopPopups;
        RamLogic.OnMemWinReached -= StopPopups;
        GameController.OnGameStarted -= StartGame;
        GameController.OnGameRestarted -= OnRestart;

    }

    IEnumerator PopUpGeneratorCoroutine() {
        while (true)
        {
            yield return new WaitForSeconds(currentPopUpSpeed);
            CreatePopUp();
            
        }
    }

    public void CreatePopUp() {
        if (startPopsGenerated && !coroutineActive)
        {
            coroutineActive = true;
            StartCoroutine(PopUpGeneratorCoroutine());
        }
        if (!allowPopUpCreation)
        {
            return;
        }
        PopUpType chosenType = SelectRandomPopUpType();

        PopUpTypeHandler newPopUpScript = null;
        if (deactivePopUps.Count > 0)
        {
            for (int i = 0; i < deactivePopUps.Count; i++)
            {
                if (deactivePopUps[i].popUpType == chosenType)
                {
                    newPopUpScript = deactivePopUps[i];
                    deactivePopUps.Remove(newPopUpScript);
                   i = deactivePopUps.Count;
                }
            }
            if (!newPopUpScript)
            {
                newPopUpScript = InstantiateNewPopUp(SelectPrefab(chosenType));
            }
           

        }
        else
        {
            newPopUpScript = InstantiateNewPopUp(SelectPrefab(chosenType));
        }
        if (newPopUpScript)
        {
            activePopUps.Add(newPopUpScript);
            newPopUpScript.transform.SetParent(this.transform);
            //newPopUpScript.transform.localPosition = new Vector3(Random.Range(-posChange, posChange), Random.Range(-posChange, posChange), 0);

            numberOfPopUps++;
            newPopUpScript.OpenRandomPopUp();
            OnPopUpOpened?.Invoke(newPopUpScript.chosenPopUp.ramConsumption);

            currentPopUpSpeed = Mathf.Clamp(currentPopUpSpeed += popUpSpeedReduction, fastestPopUpSpeed, slowestPopUpSpeed);
        }
        
    }

    PopUpTypeHandler InstantiateNewPopUp(GameObject prefab) {

        if (prefab)
        {
            GameObject newPopUp = Instantiate(prefab);
            PopUpTypeHandler newPopUpScript = newPopUp.GetComponent<PopUpTypeHandler>();
            newPopUpScript.SetUp(this);
            return newPopUpScript;
        }
        return null;
    }

    GameObject SelectPrefab(PopUpType type) {
        foreach (var item in popUpPrefabs)
        {
            if (item.GetComponent<PopUpTypeHandler>().popUpType == type)
            {
                
                return item;
            }
        }
        return null;
    }

    PopUpType SelectRandomPopUpType() {
        PopUpType chosenType = PopUpType.Image;
        int randomValue = Random.Range(0, System.Enum.GetValues(typeof(PopUpType)).Length);
        switch (randomValue)
        {
            case 0:
                chosenType = PopUpType.Image;
                break;
            case 1:
                chosenType = PopUpType.YesNo;
                break;
            case 2:
                chosenType = PopUpType.OK;
                break;
            default:
                break;
        }

        return chosenType;
    }

    public void RemovePopUp() {
        if (activePopUps.Count > 0)
        {
            int rndIndex = Random.Range(0, activePopUps.Count);
            activePopUps[rndIndex].ClosePopUp();
        }
        
    }

    public void PopUpClosed(PopUpTypeHandler popUpScript, float amount) {
        if (startPopsGenerated && !coroutineActive)
        {
            coroutineActive = true;
            StartCoroutine(PopUpGeneratorCoroutine());
        }
        if (allowPopUpCreation)
        {
            numberOfPopUps--;
            OnPopUpClosed?.Invoke(amount);
            activePopUps.Remove(popUpScript);
            deactivePopUps.Add(popUpScript);
        }
        
        
    }

    void StopPopups() {
        StopAllCoroutines();
        allowPopUpCreation = false;
        CloseAllPopUps();
    }

    void CloseAllPopUps() {
        foreach (var item in activePopUps)
        {
            item.ClosePopUp();
        }
    }

    public void OnRestart() {
        StopPopups();
        coroutineActive = false;
        startPopsGenerated = false;
    }
}
