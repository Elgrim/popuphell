﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class YesNoPopUp : PopUp
{
    [SerializeField]
    private int amountOfNewPopUps = 3;

    [SerializeField]
    private GameObject subPanel = null;

    [SerializeField]
    private List<Transform> buttons = new List<Transform>();
    
    private List<Vector3> buttonLoc = new List<Vector3>();


    public override void SetUp(PopUpGenerator generator, PopUpTypeHandler handler)
    {
        foreach (var item in buttons)
        {
            buttonLoc.Add(item.localPosition);
        }
        base.SetUp(generator, handler);
    }
    public override void OpenPopUp()
    {
        if(subPanel) subPanel.SetActive(false);
        RandomizeButtons();
        base.OpenPopUp();
    }

    void RandomizeButtons() {
        List<Vector3> tempLoc = buttonLoc;
        for (int i = 0; i < buttons.Count; i++)
        {
            if (tempLoc.Count > 0)
            {
                int rndLoc = Random.Range(0, tempLoc.Count);
                buttons[i].localPosition = tempLoc[rndLoc];
                tempLoc.RemoveAt(rndLoc);
            }
           
        }
    }

    public override void ActivateBadResult()
    {
        for (int i = 0; i < amountOfNewPopUps; i++)
        {
            popUpGenerator.CreatePopUp();
        }
        base.ClosePopUp();
    }

    public void OpenSubPanel() {
        subPanel.SetActive(true);
    }

    public void CloseSubPanel() {
        subPanel.SetActive(false);
        base.ClosePopUp();
    }
}
