﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PopUpType { Image, OK, YesNo }

public class PopUpTypeHandler : MonoBehaviour
{
    public PopUpType popUpType;

    [SerializeField]
    private List<PopUp> popUps = new List<PopUp>();
    private int chosenIndex;
    [HideInInspector]
    public PopUp chosenPopUp;

    public void SetUp(PopUpGenerator generator) {
        foreach (var item in popUps)
        {
            item.SetUp(generator, this);
        }
    }

    public void OpenRandomPopUp()
    {
        chosenIndex = Random.Range(0, popUps.Count);
        chosenPopUp = popUps[chosenIndex];
        popUps[chosenIndex].OpenPopUp();
        transform.localScale = new Vector3(1, 1, 1);
        gameObject.SetActive(true);

    }

    public void ClosePopUp() {
        
        chosenPopUp.ClosePopUp();
    }

}
