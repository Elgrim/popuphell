﻿using System.Collections.Generic;
using UnityEngine;



public class PopUp : MonoBehaviour
{
    public float ramConsumption;
    [HideInInspector]
    public  PopUpGenerator popUpGenerator;
    private PopUpTypeHandler thisTypeHandler;

    [SerializeField]
    private Vector2 xPosLimits = new Vector2(-50,50);
    [SerializeField]
    private Vector2 yPosLimits = new Vector2(-50, 50);

    [Header ("Audio")]
    private AudioManager audioManager;
    [SerializeField]
    private AudioClip popUpSound = null;
    [SerializeField]
    private float volume = 0.5f;


    public virtual void SetUp(PopUpGenerator generator, PopUpTypeHandler handler) {
        gameObject.SetActive(false);
        popUpGenerator = generator;
        thisTypeHandler = handler;
        audioManager = GameObject.FindGameObjectWithTag("AudioManager").GetComponent<AudioManager>();
    }

    public virtual void OpenPopUp()
    {
        thisTypeHandler.transform.localPosition = new Vector3(Random.Range(xPosLimits.x, xPosLimits.y), Random.Range(yPosLimits.x, yPosLimits.y), 0);
        gameObject.SetActive(true);
        if(audioManager) audioManager.PlayOneShotWithVolumeChange(popUpSound, volume);
    }

    public virtual void ActivateGoodResult()
    {
        popUpGenerator.RemovePopUp();
        ClosePopUp();
    }

    public virtual void ActivateBadResult()
    {
        popUpGenerator.CreatePopUp();
    }

    public virtual void ClosePopUp()
    {
        thisTypeHandler.transform.SetParent(null);
        gameObject.SetActive(false);
        popUpGenerator.PopUpClosed(thisTypeHandler, ramConsumption);
        thisTypeHandler.gameObject.SetActive(false);
    }
}
