﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MouseControl : MonoBehaviour
{

    [SerializeField]
    private float updateInterval = 0.2f;

    [SerializeField]
    private Sprite defaultMouseSprite = null;

    private Image mouseImage;
    [SerializeField]
    private GraphicRaycaster raycaster = null;
    PointerEventData pointerEventData;
    [SerializeField]
    private EventSystem eventSystem = null;

    private GameObject latestHitObject;

    private bool inMenu = true;

    private AudioManager audioManager;
    [SerializeField]
    private AudioClip clickSound = null;

    // Start is called before the first frame update
    void Start()
    {
        audioManager = GameObject.FindGameObjectWithTag("AudioManager").GetComponent<AudioManager>();
        mouseImage = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Cursor.visible)
        {
            Cursor.visible = false;
        }
        if (inMenu)
        {
            CalculateMousPos();
        }
        if (Input.GetMouseButtonDown(0))
        {
            if (audioManager) audioManager.PlayOneShot(clickSound);
        }

    }

    private void OnEnable()
    {
        GameController.OnGameStarted += StartGame;
        GameController.OnGameRestarted += OnRestart;
    }
    private void OnDisable()
    {
        GameController.OnGameStarted -= StartGame;
        GameController.OnGameRestarted -= OnRestart;
    }

    void StartGame() {
        inMenu = false;
        StartCoroutine(MouseFollowByPosUpdates());
    }

    void OnRestart() {
        inMenu = true;
        StopAllCoroutines();
    }

    

    IEnumerator MouseFollowByPosUpdates() {

        while (true)
        {
            CalculateMousPos();
            yield return new WaitForSeconds(updateInterval);
            if(raycaster && eventSystem) RaycastPosition();
        }
    }

    void CalculateMousPos() {
        //transform.position = Input.mousePosition;
        Vector3 screenPoint = Input.mousePosition;
        screenPoint.z = 10.0f; //distance of the plane from the camera
        transform.position = Camera.main.ScreenToWorldPoint(screenPoint);
    }

    void RaycastPosition() {
        //Set up the new Pointer Event
        pointerEventData = new PointerEventData(eventSystem);
        //Set the Pointer Event Position to that of the mouse position
        pointerEventData.position = Input.mousePosition;

        //Create a list of Raycast Results
        List<RaycastResult> results = new List<RaycastResult>();

        //Raycast using the Graphics Raycaster and mouse click position
        raycaster.Raycast(pointerEventData, results);

        if (results.Count > 0)
        {
            if (!latestHitObject || latestHitObject != results[0].gameObject)
            {
                
                latestHitObject = results[0].gameObject;
                //Debug.Log(latestHitObject.name);
                if (results[0].gameObject.tag == "MouseOverArea")
                {
                    if (latestHitObject.GetComponent<MouseChangeArea>())
                    {
                        mouseImage.sprite = latestHitObject.GetComponent<MouseChangeArea>().newMouseIcon;
                        

                    }
                    
                }
                else
                {
                    mouseImage.sprite = defaultMouseSprite;
                }
            }
            
        }
    }
}
