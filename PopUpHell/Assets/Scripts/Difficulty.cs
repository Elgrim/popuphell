﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Difficulty", menuName = "ScriptableObjects/DifficultySetting", order = 1)]
public class Difficulty : ScriptableObject
{
    public string difficultyName;

    [Header("Audio")]
    public AudioClip difficultyBgm;

    [Header("Popup values")]
    public int popUpsAtStart;
    public float startPopUpSpeed;
    public float popUpSpeedReduction;
    public float fastestPopUpSpeed;
    public float slowestPopUpSpeed;

    [Header("Popup prefabs")]
    public List<GameObject> popUpPrefabs;

    [Header("Audio")]
    public Sprite startScreen;
    public Sprite winScreen;

}
