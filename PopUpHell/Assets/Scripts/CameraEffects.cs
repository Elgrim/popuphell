﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class CameraEffects : MonoBehaviour
{

    [SerializeField]
    private Camera mainCamera = null;
    private PostProcessLayer postlayer;
    private void OnEnable()
    {
        GameController.LookedAtPC += EnableEffects;
        GameController.PowerOffEvent += DisableEffects;
    }
    private void OnDisable()
    {
        GameController.LookedAtPC -= EnableEffects;
        GameController.PowerOffEvent -= DisableEffects;

    }

    private void EnableEffects()
    {
        postlayer.enabled = true;
    }
    private void DisableEffects()
    {
        postlayer.enabled = false;

    }

    // Start is called before the first frame update
    void Start()
    {
        postlayer = mainCamera.GetComponent<PostProcessLayer>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
